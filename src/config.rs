// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::Utc;
use crates::crypto::{sha1, sha2};
use crates::crypto::hmac::Hmac;
use crates::crypto::mac::{Mac, MacResult};
use crates::hex::FromHex;
use crates::iron::Headers;
use crates::rand::{self, Rng};
use crates::serde_json::{self, Value};

use std::collections::hash_map::HashMap;
use std::error::Error;
use std::fs::File;
use std::path::{Path, PathBuf};

#[derive(Deserialize, Debug, Clone)]
struct Filter {
    kind: String,

    #[serde(default)]
    have_keys: Vec<String>,
    #[serde(default)]
    items: HashMap<String, Value>,

    header_value: Option<String>,
}

impl Filter {
    fn is_match(&self, header: &Option<String>, object: &Value) -> bool {
        if let (&Some(ref actual), &Some(ref expected)) = (header, &self.header_value) {
            return actual == expected;
        }

        for key in &self.have_keys {
            if object.pointer(key).is_none() {
                return false;
            }
        }

        for (pointer, expected) in &self.items {
            let matches = object.pointer(pointer)
                .map(|value| value == expected);

            if !matches.unwrap_or(false) {
                return false;
            }
        }

        true
    }
}

#[derive(Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum Algorithm {
    #[serde(rename = "sha1")]
    Sha1,
    #[serde(rename = "sha256")]
    Sha256,
}

impl Algorithm {
    fn mac_with(&self, data: &[u8]) -> Box<Mac> {
        match *self {
            Algorithm::Sha1 => {
                Box::new(Hmac::new(sha1::Sha1::new(), data))
            },
            Algorithm::Sha256 => {
                Box::new(Hmac::new(sha2::Sha256::new(), data))
            },
        }
    }
}

#[derive(Deserialize, Debug, Clone)]
#[serde(tag = "type")]
pub enum Comparison {
    #[serde(rename = "hmac")]
    Hmac {
        algorithm: Algorithm,
        prefix: String,
    },
    #[serde(rename = "token")]
    Token,
}

impl Comparison {
    fn verify(&self, given: String, secret: &str, data: &[u8]) -> bool {
        match *self {
            Comparison::Token => {
                given == secret
            },
            Comparison::Hmac { ref algorithm, ref prefix } => {
                if given.starts_with(prefix) {
                    if let Ok(digest) = Vec::<u8>::from_hex(given[prefix.len()..].as_bytes()) {
                        let expected = MacResult::new(&digest);
                        let mut mac = algorithm.mac_with(secret.as_ref());
                        mac.input(data);

                        expected == mac.result()
                    } else {
                        false
                    }
                } else {
                    false
                }
            },
        }
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct Verification {
    secret_key_lookup: String,
    verification_header: String,
    compare: Comparison,
}

impl Verification {
    fn verify(&self, headers: &Headers, secret: &str, data: &[u8]) -> bool {
        headers.iter()
            .find(|header| header.name() == self.verification_header)
            .map_or(false, |header| {
                self.compare.verify(header.value_string(), secret, data)
            })
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct Handler {
    path: String,
    filters: Vec<Filter>,
    header_name: Option<String>,

    verification: Option<Verification>,
}

impl Handler {
    pub fn lookup_secret<'a>(&self, secrets: &'a Value, object: &Value) -> Option<&'a str> {
        self.verification
            .as_ref()
            .and_then(|verification| {
                object.pointer(&verification.secret_key_lookup)
                    .and_then(Value::as_str)
                    .and_then(|key| secrets.get(key))
                    .and_then(Value::as_str)
            })
    }

    pub fn verify(&self, headers: &Headers, secret: Option<&str>, data: &[u8]) -> bool {
        if let Some(ref verification) = self.verification {
            secret.map_or(false, |secret_value| {
                    verification.verify(headers, secret_value, data)
                })
        } else {
            true
        }
    }

    pub fn kind(&self, headers: &Headers, object: &Value) -> Option<&str> {
        let header = self.header_name
            .as_ref()
            .and_then(|name| {
                headers.iter()
                    .find(|header| header.name() == name)
                    .map(|header| {
                        header.value_string()
                    })
            });

        for filter in &self.filters {
            if filter.is_match(&header, object) {
                info!("matched an event of kind {}", filter.kind);

                return Some(&filter.kind);
            }
        }

        None
    }

    pub fn write_object(&self, kind: &str, object: Value) -> Result<(), Box<Error>> {
        let rndpart = rand::thread_rng()
            .gen_ascii_chars()
            .take(12)
            .collect::<String>();
        let filename = format!("{}-{}.json", Utc::now().to_rfc3339(), rndpart);

        let mut filepath = PathBuf::from(&self.path);
        filepath.push(filename);

        debug!("writing an event of kind {} to {}",
               kind,
               filepath.display());

        let mut fout = File::create(&filepath)?;

        let output = json!({
            "kind": kind,
            "data": object
        });

        Ok(serde_json::to_writer(&mut fout, &output)?)
    }
}

pub type HandlerMap = HashMap<String, Handler>;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub handlers: HandlerMap,
    pub secrets: Option<String>,
}

impl Config {
    /// Read the configuration from a path.
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, Box<Error>> {
        let fin = File::open(path.as_ref())?;
        Ok(serde_json::from_reader(fin)?)
    }

    pub fn secrets(&self) -> Result<Value, Box<Error>> {
        Ok(if let Some(ref path) = self.secrets {
            let fin = File::open(path)?;
            serde_json::from_reader(fin)?
        } else {
            Value::Null
        })
    }
}

#[cfg(test)]
mod test {
    use crates::iron::Headers;
    use crates::serde_json::Value;

    use config::{Algorithm, Comparison, Filter, Handler, Verification};

    use std::collections::hash_map::HashMap;

    const WEBHOOK_HEADER_NAME: &str = "X-Webhook-Listen-Kind";
    const VERIFICATION_HEADER: &str = "X-Webhook-Listen-Verification";

    fn create_handler<'a, I>(filters: I) -> Handler
        where I: IntoIterator<Item = &'a Filter>,
    {
        Handler {
            path: String::new(),
            filters: filters.into_iter().cloned().collect(),
            header_name: Some(WEBHOOK_HEADER_NAME.to_string()),
            verification: None,
        }
    }

    #[test]
    fn test_empty_filter() {
        let handler = create_handler(&[
            Filter {
                kind: "empty".to_string(),
                have_keys: Vec::new(),
                items: HashMap::new(),
                header_value: None,
            },
        ]);

        let empty = json!({});
        let array = json!({
            "array": [0],
        });
        let array_non_array = json!({
            "array": 0,
        });
        let with_slash = json!({
            "with/slash": 0,
        });
        let with_tilde = json!({
            "with~tilde": 0,
        });
        let string_keys = json!({
            "dict": {
                "0": 0,
            },
        });
        let string_keys_missing = json!({
            "dict": {
                "1": 0,
            },
        });
        let many_keys = json!({
            "first_key": 0,
            "second_key": 0,
        });
        let many_keys_missing = json!({
            "first_key": 0,
        });
        let all = json!({
            "array": [0],
            "with/slash": 0,
            "with~tilde": 0,
            "dict": {
                "0": 0,
            },
            "first_key": 0,
            "second_key": 0,
        });

        let headers = Headers::new();

        assert_eq!(handler.kind(&headers, &empty), Some("empty"));
        assert_eq!(handler.kind(&headers, &array), Some("empty"));
        assert_eq!(handler.kind(&headers, &array_non_array), Some("empty"));
        assert_eq!(handler.kind(&headers, &with_slash), Some("empty"));
        assert_eq!(handler.kind(&headers, &with_tilde), Some("empty"));
        assert_eq!(handler.kind(&headers, &string_keys), Some("empty"));
        assert_eq!(handler.kind(&headers, &string_keys_missing), Some("empty"));
        assert_eq!(handler.kind(&headers, &many_keys), Some("empty"));
        assert_eq!(handler.kind(&headers, &many_keys_missing), Some("empty"));
        assert_eq!(handler.kind(&headers, &all), Some("empty"));
    }

    #[test]
    fn test_have_keys() {
        let handler = create_handler(&[
            Filter {
                kind: "array".to_string(),
                have_keys: vec![
                    "/array/0".to_string(),
                ],
                items: HashMap::new(),
                header_value: None,
            },
            Filter {
                kind: "with_slash".to_string(),
                have_keys: vec![
                    "/with~1slash".to_string(),
                ],
                items: HashMap::new(),
                header_value: None,
            },
            Filter {
                kind: "with_tilde".to_string(),
                have_keys: vec![
                    "/with~0tilde".to_string(),
                ],
                items: HashMap::new(),
                header_value: None,
            },
            Filter {
                kind: "string_keys".to_string(),
                have_keys: vec![
                    "/dict/0".to_string(),
                ],
                items: HashMap::new(),
                header_value: None,
            },
            Filter {
                kind: "many_keys".to_string(),
                have_keys: vec![
                    "/first_key".to_string(),
                    "/second_key".to_string(),
                ],
                items: HashMap::new(),
                header_value: None,
            },
        ]);

        let empty = json!({});
        let array = json!({
            "array": [0],
        });
        let array_non_array = json!({
            "array": 0,
        });
        let with_slash = json!({
            "with/slash": 0,
        });
        let with_tilde = json!({
            "with~tilde": 0,
        });
        let string_keys = json!({
            "dict": {
                "0": 0,
            },
        });
        let string_keys_missing = json!({
            "dict": {
                "1": 0,
            },
        });
        let many_keys = json!({
            "first_key": 0,
            "second_key": 0,
        });
        let many_keys_missing = json!({
            "first_key": 0,
        });
        let all = json!({
            "array": [0],
            "with/slash": 0,
            "with~tilde": 0,
            "dict": {
                "0": 0,
            },
            "first_key": 0,
            "second_key": 0,
        });

        let headers = Headers::new();

        assert_eq!(handler.kind(&headers, &empty), None);
        assert_eq!(handler.kind(&headers, &array), Some("array"));
        assert_eq!(handler.kind(&headers, &array_non_array), None);
        assert_eq!(handler.kind(&headers, &with_slash), Some("with_slash"));
        assert_eq!(handler.kind(&headers, &with_tilde), Some("with_tilde"));
        assert_eq!(handler.kind(&headers, &string_keys), Some("string_keys"));
        assert_eq!(handler.kind(&headers, &string_keys_missing), None);
        assert_eq!(handler.kind(&headers, &many_keys), Some("many_keys"));
        assert_eq!(handler.kind(&headers, &many_keys_missing), None);
        assert_eq!(handler.kind(&headers, &all), Some("array"));
    }

    #[test]
    fn test_items() {
        let handler = create_handler(&[
            Filter {
                kind: "multi".to_string(),
                have_keys: Vec::new(),
                items: [
                    ("/number".to_string(), json!(1)),
                    ("/null".to_string(), Value::Null),
                ].iter().cloned().collect(),
                header_value: None,
            },
            Filter {
                kind: "null".to_string(),
                have_keys: Vec::new(),
                items: [
                    ("/null".to_string(), Value::Null),
                ].iter().cloned().collect(),
                header_value: None,
            },
            Filter {
                kind: "number".to_string(),
                have_keys: Vec::new(),
                items: [
                    ("/number".to_string(), json!(0)),
                ].iter().cloned().collect(),
                header_value: None,
            },
            Filter {
                kind: "string".to_string(),
                have_keys: Vec::new(),
                items: [
                    ("/string".to_string(), json!("string")),
                ].iter().cloned().collect(),
                header_value: None,
            },
            Filter {
                kind: "array".to_string(),
                have_keys: Vec::new(),
                items: [
                    ("/array".to_string(), json!([0])),
                ].iter().cloned().collect(),
                header_value: None,
            },
            Filter {
                kind: "dict".to_string(),
                have_keys: Vec::new(),
                items: [
                    ("/dict".to_string(), json!({"0": 0})),
                ].iter().cloned().collect(),
                header_value: None,
            },
        ]);

        let empty = json!({});
        let null = json!({
            "null": Value::Null,
        });
        let null_mismatch = json!({
            "null": 0,
        });
        let number = json!({
            "number": 0,
        });
        let number_mismatch = json!({
            "number": 1,
        });
        let string = json!({
            "string": "string",
        });
        let string_mismatch = json!({
            "string": "mismatch",
        });
        let array = json!({
            "array": [0],
        });
        let array_mismatch = json!({
            "array": 0,
        });
        let dict = json!({
            "dict": {
                "0": 0,
            },
        });
        let dict_mismatch = json!({
            "dict": {
                "1": 0,
            },
        });
        let multi = json!({
            "null": Value::Null,
            "number": 1,
        });
        let multi_missing = json!({
            "number": 1,
        });

        let headers = Headers::new();

        assert_eq!(handler.kind(&headers, &empty), None);
        assert_eq!(handler.kind(&headers, &null), Some("null"));
        assert_eq!(handler.kind(&headers, &null_mismatch), None);
        assert_eq!(handler.kind(&headers, &number), Some("number"));
        assert_eq!(handler.kind(&headers, &number_mismatch), None);
        assert_eq!(handler.kind(&headers, &string), Some("string"));
        assert_eq!(handler.kind(&headers, &string_mismatch), None);
        assert_eq!(handler.kind(&headers, &array), Some("array"));
        assert_eq!(handler.kind(&headers, &array_mismatch), None);
        assert_eq!(handler.kind(&headers, &dict), Some("dict"));
        assert_eq!(handler.kind(&headers, &dict_mismatch), None);
        assert_eq!(handler.kind(&headers, &multi), Some("multi"));
        assert_eq!(handler.kind(&headers, &multi_missing), None);
    }

    #[test]
    fn test_have_keys_and_items() {
        let handler = create_handler(&[
            Filter {
                kind: "match".to_string(),
                have_keys: vec![
                    "/array".to_string(),
                ],
                items: [
                    ("/null".to_string(), Value::Null),
                ].iter().cloned().collect(),
                header_value: None,
            },
        ]);

        let empty = json!({});
        let has_keys = json!({
            "array": 0,
        });
        let has_item = json!({
            "null": Value::Null,
        });
        let has_everything = json!({
            "array": 0,
            "null": Value::Null,
        });
        let has_mismatch = json!({
            "array": 0,
            "null": 0,
        });

        let headers = Headers::new();

        assert_eq!(handler.kind(&headers, &empty), None);
        assert_eq!(handler.kind(&headers, &has_keys), None);
        assert_eq!(handler.kind(&headers, &has_item), None);
        assert_eq!(handler.kind(&headers, &has_everything), Some("match"));
        assert_eq!(handler.kind(&headers, &has_mismatch), None);
    }

    #[test]
    fn test_headers() {
        let handler = create_handler(&[
            Filter {
                kind: "also_keys".to_string(),
                have_keys: vec![
                    "/array".to_string(),
                ],
                items: HashMap::new(),
                header_value: Some("also_keys".to_string()),
            },
            Filter {
                kind: "multi".to_string(),
                have_keys: Vec::new(),
                items: HashMap::new(),
                header_value: Some("multi".to_string()),
            },
        ]);

        let empty = json!({});
        let array = json!({
            "array": [0],
        });

        let mut headers = Headers::new();

        assert_eq!(handler.kind(&headers, &empty), Some("multi"));
        assert_eq!(handler.kind(&headers, &array), Some("also_keys"));

        headers.set_raw(WEBHOOK_HEADER_NAME, vec!["multi".bytes().collect()]);

        assert_eq!(handler.kind(&headers, &empty), Some("multi"));
        assert_eq!(handler.kind(&headers, &array), Some("multi"));

        headers.set_raw(WEBHOOK_HEADER_NAME, vec!["also_keys".bytes().collect()]);

        assert_eq!(handler.kind(&headers, &empty), Some("also_keys"));
        assert_eq!(handler.kind(&headers, &array), Some("also_keys"));

        headers.set_raw(WEBHOOK_HEADER_NAME, vec!["unmatched".bytes().collect()]);

        assert_eq!(handler.kind(&headers, &empty), None);
        assert_eq!(handler.kind(&headers, &array), None);
    }

    #[test]
    fn test_verification_lookup_secret() {
        let mut handler = create_handler(&[]);

        let no_secrets = json!({});
        let with_secret = json!({
            "key": "secret",
        });
        let data = json!({
            "secret_key": "key",
        });

        assert_eq!(handler.lookup_secret(&no_secrets, &data), None);
        assert_eq!(handler.lookup_secret(&with_secret, &data), None);

        handler.verification = Some(Verification {
            secret_key_lookup: "/secret_key".to_string(),
            verification_header: VERIFICATION_HEADER.to_string(),
            compare: Comparison::Token,
        });

        assert_eq!(handler.lookup_secret(&no_secrets, &data), None);
        assert_eq!(handler.lookup_secret(&with_secret, &data), Some("secret"));
    }

    #[test]
    fn test_verification_no_header() {
        let mut handler = create_handler(&[]);

        handler.verification = Some(Verification {
            secret_key_lookup: "/secret".to_string(),
            verification_header: VERIFICATION_HEADER.to_string(),
            compare: Comparison::Token,
        });

        let data = Vec::new();
        let headers = Headers::new();

        assert_eq!(handler.verify(&headers, None, &data), false);
        assert_eq!(handler.verify(&headers, Some("secret"), &data), false);
    }

    #[test]
    fn test_verification_token() {
        let mut handler = create_handler(&[]);

        handler.verification = Some(Verification {
            secret_key_lookup: "/secret".to_string(),
            verification_header: VERIFICATION_HEADER.to_string(),
            compare: Comparison::Token,
        });

        let data = Vec::new();
        let mut headers = Headers::new();

        headers.set_raw(VERIFICATION_HEADER, vec!["secret".bytes().collect()]);

        assert_eq!(handler.verify(&headers, None, &data), false);
        assert_eq!(handler.verify(&headers, Some("secret"), &data), true);
    }

    #[test]
    fn test_verification_hmac_sha1() {
        let mut handler = create_handler(&[]);

        handler.verification = Some(Verification {
            secret_key_lookup: "/secret".to_string(),
            verification_header: VERIFICATION_HEADER.to_string(),
            compare: Comparison::Hmac {
                algorithm: Algorithm::Sha1,
                prefix: String::new(),
            },
        });

        let data = Vec::new();
        let mut headers = Headers::new();

        headers.set_raw(VERIFICATION_HEADER, vec!["25af6174a0fcecc4d346680a72b7ce644b9a88e8".bytes().collect()]);

        assert_eq!(handler.verify(&headers, None, &data), false);
        assert_eq!(handler.verify(&headers, Some("secret"), &data), true);
        assert_eq!(handler.verify(&headers, Some("wrong_secret"), &data), false);
    }

    #[test]
    fn test_verification_hmac_sha256() {
        let mut handler = create_handler(&[]);

        handler.verification = Some(Verification {
            secret_key_lookup: "/secret".to_string(),
            verification_header: VERIFICATION_HEADER.to_string(),
            compare: Comparison::Hmac {
                algorithm: Algorithm::Sha256,
                prefix: String::new(),
            },
        });

        let data = Vec::new();
        let mut headers = Headers::new();

        headers.set_raw(VERIFICATION_HEADER, vec!["f9e66e179b6747ae54108f82f8ade8b3c25d76fd30afde6c395822c530196169".bytes().collect()]);

        assert_eq!(handler.verify(&headers, None, &data), false);
        assert_eq!(handler.verify(&headers, Some("secret"), &data), true);
        assert_eq!(handler.verify(&headers, Some("wrong_secret"), &data), false);
    }

    #[test]
    fn test_verification_hmac_prefix() {
        let mut handler = create_handler(&[]);

        handler.verification = Some(Verification {
            secret_key_lookup: "/secret".to_string(),
            verification_header: VERIFICATION_HEADER.to_string(),
            compare: Comparison::Hmac {
                algorithm: Algorithm::Sha1,
                prefix: "sha1=".to_string(),
            },
        });

        let data = Vec::new();
        let mut headers = Headers::new();

        headers.set_raw(VERIFICATION_HEADER, vec!["sha1=25af6174a0fcecc4d346680a72b7ce644b9a88e8".bytes().collect()]);

        assert_eq!(handler.verify(&headers, None, &data), false);
        assert_eq!(handler.verify(&headers, Some("secret"), &data), true);
        assert_eq!(handler.verify(&headers, Some("wrong_secret"), &data), false);
    }
}
