// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Webhook listener
//!
//! This program listens over HTTP for JSON webhook events to pass along to a handler.
//!
//! See the [usage](usage.html) documentation for more.

#![warn(missing_docs)]

#[macro_use]
extern crate clap;

#[macro_use]
extern crate log;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate serde_json;

mod crates {
    pub extern crate chrono;
    pub extern crate clap;
    pub extern crate crypto;
    pub extern crate hex;
    pub extern crate iron;
    pub extern crate itertools;
    pub extern crate log;
    pub extern crate rand;
    pub extern crate reqwest;
    pub extern crate serde_json;
    pub extern crate systemd;
}

use crates::clap::{App, Arg};
use crates::iron::Iron;
use crates::log::LogLevel;
use crates::reqwest::{Client, Method};
use crates::systemd::journal::JournalLog;

mod config;
use config::Config;

mod router;
use router::Router;

use std::error::Error;
use std::path::Path;

/// Run on an address based on a configuration at the given path.
fn run_from_config(address: &str, config_path: &Path) -> Result<(), Box<Error>> {
    let config = Config::from_path(&config_path)?;
    let router = Router::new(config_path, config)?;

    let _ = Iron::new(router).http(address)?;

    Ok(())
}

/// A `main` function which supports `try!`.
fn try_main() -> Result<(), Box<Error>> {
    let matches = App::new("webhook-listen")
        .version(crate_version!())
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Listen over HTTP for JSON webhook events to pass along to a handler")
        .arg(Arg::with_name("ADDRESS")
            .short("a")
            .long("address")
            .help("The address to listen on")
            .required(true)
            .takes_value(true))
        .arg(Arg::with_name("CONFIG")
            .short("c")
            .long("config")
            .help("Path to the configuration file")
            .required(true)
            .takes_value(true))
        .arg(Arg::with_name("DEBUG")
            .short("d")
            .long("debug")
            .help("Increase verbosity")
            .multiple(true))
        .arg(Arg::with_name("VERIFY")
            .short("v")
            .long("verify")
            .help("Check the configuration file and exit"))
        .arg(Arg::with_name("RELOAD")
            .short("r")
            .long("reload")
            .help("Reload the configuration"))
        .get_matches();

    let log_level = match matches.occurrences_of("DEBUG") {
        0 => LogLevel::Error,
        1 => LogLevel::Warn,
        2 => LogLevel::Info,
        3 => LogLevel::Debug,
        4 | _ => LogLevel::Trace,
    };
    JournalLog::init_with_level(log_level.to_log_level_filter())?;

    let config_path = Path::new(matches.value_of("CONFIG")
        .expect("the configuration option is required"));

    if matches.is_present("VERIFY") {
        Config::from_path(&config_path)?;
    } else if matches.is_present("RELOAD") {
        let address = matches.value_of("ADDRESS").expect("the address option is required");
        let url = format!("http://{}/__reload", address);
        let client = Client::new();
        let response = client.request(Method::Put, &url).send()?;
        assert!(response.status().is_success(),
                "Error occurred when reloading the service.");
    } else {
        run_from_config(matches.value_of("ADDRESS").expect("the address option is required"),
                        config_path)?;
    }

    Ok(())
}

/// The entry point.
///
/// Wraps around `try_main` to panic on errors.
fn main() {
    if let Err(err) = try_main() {
        panic!("{:?}", err);
    }
}
