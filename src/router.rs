// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::iron::{Handler, Headers, IronResult, Request, Response};
use crates::iron::method::Method;
use crates::iron::status;
use crates::itertools::Itertools;
use crates::serde_json::{self, Value};

use config::Config;

use std::error::Error;
use std::io::{self, ErrorKind, Read};
use std::path::{Path, PathBuf};
use std::sync::RwLock;

/// A router for the Iron framework.
///
/// Drops JSON objects which are received via `POST` into a directory using the current date as the
/// filename.
pub struct Router {
    /// The path to the configuration file.
    path: PathBuf,
    /// The configuration for sorting objects based on their type.
    config: RwLock<Config>,
    /// The secrets for various projects.
    secrets: RwLock<Value>,
}

impl Router {
    /// Create a new router for a path.
    pub fn new<P: AsRef<Path>>(path: P, config: Config) -> Result<Self, Box<Error>> {
        Ok(Self {
            path: path.as_ref().to_path_buf(),

            secrets: RwLock::new(config.secrets()?),
            config: RwLock::new(config),
        })
    }

    /// Reload the filename.
    fn reload(&self) -> Response {
        match Config::from_path(&self.path) {
            Ok(config) => {
                {
                    let mut inner_config = self.config
                        .write()
                        .expect("expected to be able to get a write lock on the configuration");

                    *inner_config = config;
                }

                self.reload_secrets()
            },
            Err(err) => Response::with((status::NotAcceptable, format!("{:?}", err))),
        }
    }

    /// Reload secrets.
    fn reload_secrets(&self) -> Response {
        let config = self.config
            .read()
            .expect("expected to be able to get a read lock on the configuration");

        match config.secrets() {
            Ok(secrets) => {
                let mut inner_secrets = self.secrets
                    .write()
                    .expect("expected to be able to get a write lock on the secrets");

                *inner_secrets = secrets;

                Response::with(status::Ok)
            },
            Err(err) => Response::with((status::NotAcceptable, format!("{:?}", err))),
        }
    }

    /// Handle an object received over the given path.
    fn handle(&self, path: &str, headers: &Headers, data: &[u8], object: Value) -> Response {
        let config = self.config
            .read()
            .expect("expected to be able to get a read lock on the configuration");

        if let Some(handler) = config.handlers.get(path) {
            let secret = {
                let secrets = self.secrets
                    .read()
                    .expect("expected to be able to get a read lock on the secrets");

                handler.lookup_secret(&secrets, &object)
                    .map(ToString::to_string)
            };

            if !handler.verify(headers, secret.as_ref().map(AsRef::as_ref), data) {
                error!(target: "handler",
                       "failed to verify the a webhook:\n\
                        headers:\n{}\ndata:\n{:?}",
                        headers,
                        data);

                return Response::with(status::NotAcceptable);
            }

            if let Some(kind) = handler.kind(headers, &object) {
                if let Err(err) = handler.write_object(kind, object) {
                    error!(target: "handler",
                           "failed to write the {} object: {:?}",
                           kind,
                           err);
                }
            }

            Response::with(status::Accepted)
        } else {
            Response::with(status::NotFound)
        }
    }
}

impl Handler for Router {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        debug!(target: "handler",
               "got a {} request at {}",
               req.method,
               req.url.path().iter().format("/"));

        Ok(match req.method {
            Method::Put => {
                if req.url.path() == ["__reload"] {
                    self.reload()
                } else if req.url.path() == ["__reload_secrets"] {
                    self.reload_secrets()
                } else {
                    Response::with(status::NotFound)
                }
            },
            Method::Post => {
                let path = req.url.path().join("/");
                let mut data = Vec::new();

                req.body.read_to_end(&mut data)
                    .and_then(|_| {
                        serde_json::from_slice(&data)
                            .map_err(|err| {
                                io::Error::new(ErrorKind::Other, format!("{:?}", err))
                            })
                    })
                    .map(|object| {
                        self.handle(&path, &req.headers, &data, object)
                    })
                    .unwrap_or_else(|err| {
                        Response::with((status::BadRequest, format!("{:?}", err)))
                    })
            },
            _ => Response::with(status::MethodNotAllowed),
        })
    }
}
