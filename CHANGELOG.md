# v1.1.0 (unreleased)

  * Verification of received webhooks is now supported via either token or
    HMAC.

# v1.0.3

  * Update the `systemd` dependency for improved logging.

# v1.0.2

  * Update dependencies.

# v1.0.1

  * Metadata updates.
  * Improved logging output.

# v1.0.0

  * Initial stable release.
